package com.example.Student;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

interface StudentRepository extends JpaRepository<Student, Long> {

    @Query("SELECT b FROM Student b WHERE b.firstName = ?1")
    Optional<Student> findByFirstName(String firstName);

}

