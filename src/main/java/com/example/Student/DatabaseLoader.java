package com.example.Student;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
class DatabaseLoader {

    private static final Logger log = LoggerFactory.getLogger(DatabaseLoader.class);

    @Bean
    CommandLineRunner initDatabase(StudentRepository studentRepository) {

        return args -> {
            log.info("Preloading " + studentRepository.save(new Student("Kiss", "Zoltán", 15.0)));
            log.info("Preloading " + studentRepository.save(new Student("Szabó", "István", 12.0)));
            log.info("Preloading " + studentRepository.save(new Student("Nagy", "Béla", 10.0)));
            log.info("Preloading " + studentRepository.save(new Student("Kerekes", "László", 14.0)));
            log.info("Preloading " + studentRepository.save(new Student("Farkas", "Balázs", 12.0)));
        };
    }
}
