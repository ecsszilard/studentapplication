package com.example.Student;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;


@Controller
class StudentController {

    private final StudentRepository studentRepository;
    private final HistoryRepository historyRepository;

    StudentController(StudentRepository studentRepository, HistoryRepository historyRepository) {
        this.studentRepository = studentRepository;
        this.historyRepository = historyRepository;
    }

    @GetMapping("/students")
    public String getStudents(Model model) {
        model.addAttribute("actualStudents", studentRepository.findAll());
        if (historyRepository.findAll().size() % 10 == 0) {
            model.addAttribute("aggregatedHistory", historyRepository.findAll());
        }
        return "students";
    }

    @PostMapping("/updateName/{id}")
    public String updateName(@PathVariable Long id, @ModelAttribute Student newStudent, Model model) {
        Student student = studentRepository.findById(id)
                .map(Student -> {
                    Student.setFirstName(newStudent.getFirstName());
                    return studentRepository.save(Student);
                })
                .orElseThrow(() -> new StudentNotFoundException(id.toString()));
        return getStudents(model);
    }

    @GetMapping("/students/{id}")
    String getStudentById(@PathVariable Long id, Model model) {
        Student student = studentRepository.findById(id)
                .orElseThrow(() -> new StudentNotFoundException(id.toString()));
        model.addAttribute("student", student);

        return "studentDetails";
    }

    @GetMapping("/doRejuvenation")
    public String getTransactionUI(Model model) {
        model.addAttribute("rejuvenation", new Rejuvenation(0L, "HUF"));
        if (historyRepository.findAll().size() % 10 == 0) {
            model.addAttribute("aggregatedHistory", historyRepository.findAll());
        }
        return "rejuvenation";
    }

    @PostMapping("/doRejuvenation")
    public String doTransaction(@ModelAttribute Rejuvenation rejuvenation, Model model) {
        Student updatedStudent = studentRepository.findByFirstName(rejuvenation.firstName)
                .map(student -> {
                    student.setAge(student.getAge() + ((double) rejuvenation.value));
                return studentRepository.save(student);
                })
                .orElseThrow(() -> new StudentNotFoundException(rejuvenation.firstName));
        historyRepository.save(new History("The " + updatedStudent.getSurName() + "'s amount has changed with "
                + rejuvenation.value + " to " + updatedStudent.getAge()));
        return getStudents(model);
    }
}

