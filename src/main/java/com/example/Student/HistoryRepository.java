package com.example.Student;

import org.springframework.data.jpa.repository.JpaRepository;

interface HistoryRepository extends JpaRepository<History, Long> {

}

