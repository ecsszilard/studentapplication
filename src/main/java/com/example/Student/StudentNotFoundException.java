package com.example.Student;

public class StudentNotFoundException extends RuntimeException {
    public StudentNotFoundException(String name) {
        super("Could not find student " + name);
    }
}
