package com.example.Student;

import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Student {

    private @Id @GeneratedValue Long id;
    private String surName;
    private String firstName;
    private double age;

    Student() {}

    Student(String SurName, String FirstName, double age) {

        this.surName = SurName;
        this.firstName = FirstName;
        this.age = age;
    }

    public Long getId() {
        return this.id;
    }

    public String getSurName() {
        return this.surName;
    }

    public String getFirstName() {
        return this.firstName;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setSurName(String surName) {
        this.surName = surName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public double getAge() {
        return age;
    }

    public void setAge(double age) {
        this.age = age;
    }

    @Override
    public boolean equals(Object o) {

        if (this == o)
            return true;
        if (!(o instanceof Student))
            return false;
        Student Student = (com.example.Student.Student) o;
        return Objects.equals(this.id, Student.id) && Objects.equals(this.surName, Student.surName)
                && Objects.equals(this.firstName, Student.firstName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id, this.surName, this.firstName);
    }

    @Override
    public String toString() {
        return "Student{" + "id=" + this.id + ", SurName='" + this.surName + '\'' + ", FirstName='" + this.firstName + '\'' + '}';
    }
}